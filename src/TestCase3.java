import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.Driver;

public class TestCase3 {
    public static void main(String[] args) {


        WebDriver driver = Driver.getDriver();

        driver.get("https://automationexercise.com/");

        if(driver.findElement(By.cssSelector("div[class='single-widget']>h2")).isDisplayed())
            System.out.println("Subscription heading PASSED");
        else System.out.println("Subscription heading FAILED");

        if(driver.findElement(By.cssSelector("input[placeholder]")).isDisplayed()){
            System.out.println("Email address validation PASSED");
        }
        else System.out.println("Email address validation FAILED");

        if(driver.findElement(By.id("subscribe")).isDisplayed()){
            System.out.println("Subscribe button validation PASSED");
        }
        else System.out.println("Subscribe button validation FAILED");

        if(driver.findElement(By.xpath("//form[@class='searchform']/p")).getText().equals("Get the most recent updates from\n" +
                "our site and be updated your self...")){
            System.out.println("Get most recent.... validation PASSED");
        }
        else System.out.println("validation FAILED");

        Driver.quitDriver();
    }
}
