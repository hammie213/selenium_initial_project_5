import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.Driver;

import java.util.List;

public class TestCase2 {
    public static void main(String[] args) {

        WebDriver driver = Driver.getDriver();

        driver.get("https://automationexercise.com/");

        List<WebElement> headerItems = driver.findElements(By.xpath("//ul[@class='nav navbar-nav']"));


        for(WebElement header : headerItems){
            System.out.println(header.getText());
        }


        Driver.quitDriver();
    }
}
